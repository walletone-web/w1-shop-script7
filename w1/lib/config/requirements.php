<?php
return array(
  'app.installer' => array(
    'strict' => true,
    'version' => '>=1.5.14.73',
  ),
  'php' => array(
    'strict' => true,
    'version' => '>=5.4',
  ),
  'app.shop'=>array(
        'name'=>'Приложение «Магазин»',
        'description'=>'',
        'strict'=>true,
        'version'=>'>=1.5.9.32',
    ),
);
